module Main where

import Weather

-- main :: IO ()
-- main = do
--   putStrLn "hello world"

main :: IO ()
main = do
  city <- argParse
  response <- getWeather city
  case response of
    (Just weather) -> putStrLn $ showEmotion $ code weather
    Nothing -> error "Error getting Weather Information from the API."
