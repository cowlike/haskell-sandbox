{-# LANGUAGE OverloadedStrings #-}

-- A simple weather lookup service
module Weather where

import Data.Aeson (FromJSON (..), decode, withObject, (.:))
import Data.ByteString.Lazy (ByteString)
import Data.Maybe (listToMaybe)
import Data.Monoid ((<>))
import Data.Text (Text)
import Data.Text.IO (putStrLn)
import Network.HTTP.Client
--import Network.HTTP.Types.Status (statusCode)
import Prelude hiding (putStrLn)
import System.Environment (getArgs)

type URL = String
type City = String
type Code = String
type Emoticon = Text

-- Datatype for Weather (considering as a record, String)
data Weather = Weather { code :: String }

-- hs api for json aeson
-- https://hackage.haskell.org/package/aeson-0.1.0.0/docs/Data-Aeson.html[Tag::n/a]t:FromJSON
instance FromJSON Weather where
  parseJSON = withObject "Weather" $ \obj -> do
    currentWeather <- head <$> obj .: "weather"
    Weather <$> currentWeather .: "icon"

-- openweather api url
apiUrl :: URL
apiUrl = "http://api.openweathermap.org/data/2.5/weather?q="

-- API Key for Calling the openweather api
appID :: String
appID = "ee26a534deed1c1d3325decf701d5a68"

-- Build query parameters
-- eg. http://api.openweathermap.org/data/2.5/weather?q=NewYork&units=metric&appid=ee26a534deed1c1d3325decf701d5a68
urlBuilder :: City -> URL
urlBuilder city = apiUrl <> city <> "&units=metric&appid=" <> appID

-- Make a GET request
httpRequest :: URL -> IO ByteString
httpRequest url = do
  manager <- newManager defaultManagerSettings
  request <- parseUrl url
  response <- httpLbs request manager
  return (responseBody response)

-- function for getting the weather information
-- based on the city provided.
getWeather :: City -> IO (Maybe Weather)
getWeather city = decode <$> httpRequest (urlBuilder city)

-- Show an emoticon or emoji
-- unicode values for weather from https://www.emojibase.com/emojilist/weather
showEmotion :: Code -> String
showEmotion weather_code =
  if length weather_code < 2
    then "\8265\65039" -- Unknown
    else case x of
      "01" -> "\9728\65039" -- Sun
      "02" -> "\9925\65039" -- Sun with cloud
      "03" -> "\9729\65039" -- Cloud
      "04" -> "\9729\65039" -- Cloud
      "09" -> "\128166" -- Rain
      "10" -> "\128166" -- Rain
      "11" -> "\9889\65039" -- Thunder
      "13" -> "\10052\65039" -- Snow
      "50" -> "\9810\65038" -- Mist
      _ -> "\8265\65039" -- Unknown
    where x = take 2 weather_code

-- showEmotion :: Code -> String
-- showEmotion = id

-- argument handling
-- using listToMaybe from Data.Maybe
argParse :: IO City
argParse = readCity <$> getArgs
  where readCity args = case listToMaybe args of
                          Nothing -> error "No City Information provided."
                          Just s -> s

-- Main call
-- main :: IO ()
-- main = do
--   city <- argParse
--   response <- getWeather city
--   case response of
--     (Just weather) -> putStrLn $ showEmotion $ code weather
--     -- (Just weather) -> print $ showEmotion $ code weather
--     Nothing -> error "Error getting Weather Information from the API."
