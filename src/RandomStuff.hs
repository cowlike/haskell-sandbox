module RandomStuff where

import Prelude hiding (
  Maybe
  ,Nothing
  ,Just
  ,Either
  ,Left
  ,Right)

import qualified Data.Char as C

fizzbuzz :: [Int] -> [(Int,String)]
fizzbuzz = map fb where
  m x by = x `mod` by == 0
  fb x
    | m x 3 && m x 5 = (x,"Fizzbuzz")
    | m x 3 = (x,"Fizz")
    | m x 5 = (x,"Buzz")
    | otherwise = (x,"")

fb n = let f x y = mod x y == 0 in show n ++ (if f n 3 then " Fizz" else "") ++ (if f n 5 then " Buzz" else "")
fb' n = ["","","","Fizz","","Buzz","","","","","","","","","","FizzBuzz"]

fooMap :: (a -> b) -> [a] -> [b]
fooMap _ [] = []
fooMap g (x:xs) = g x : g x : fooMap g xs

data Maybe a = Nothing | Just a

instance Functor Maybe where
  fmap _ Nothing  = Nothing
  fmap g (Just a) = Just (g a)

data Either a b = Left a | Right b deriving Show

instance Functor (Either a) where
  fmap _ (Left a) = Left a
  fmap g (Right b) = Right (g b)

unzip' :: [(a,b)] -> ([a],[b])
unzip' = foldr (\v t -> (fst v : fst t, snd v : snd t)) ([],[])

unzip'' :: [(a,b)] -> ([a],[b])
unzip'' = foldr (\(v1,v2) (t1,t2) -> (v1 : t1, v2 : t2)) ([],[])

tweet :: String
tweet = "01100110 01100101 01100101 01101100 01101001 01101110 01100111 00100000 01101100 01110101 01100011 01101011 01111001 00001010"

binToChar :: String -> Char
binToChar = C.chr . foldl (\t v -> t * 2 + (C.ord v - 48)) 0

showTweet :: IO ()
showTweet = putStr $ "I'm " ++ map binToChar (words tweet)

{- database stuff
import Database.HDBC
import Database.HDBC.PostgreSQL

conn <- connectPostgreSQL "host=vcld16gdachwb01-fe.ual.com user=someuser password=somepass dbname=sonarqube"
getTables conn
quickQuery' conn "select * from users" []
-}
