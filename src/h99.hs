module H99 where

import qualified Data.List as L

-- https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems
-- 1
myLast :: [a] -> a
myLast = head . reverse

-- 2
myButLast :: [a] -> a
myButLast = head . drop 1 . reverse

-- 3
elementAt :: [a] -> Int -> a
elementAt xs k = head $ drop (k - 1) xs

-- 4
myLength :: [a] -> Int
myLength = foldr (\_ acc -> acc + 1) 0

-- 5
myReverse :: String -> String
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

-- 6
isPalindrome :: String -> Bool
isPalindrome s = s == myReverse s

-- 7
data NestedList a = Elem a | List [NestedList a]
flatten :: NestedList a -> [a]
flatten (List []) = []
flatten (Elem a) = [a]
flatten (List (x : xs)) = flatten x ++ flatten (List xs)

-- 8
compress :: Eq a => [a] -> [a]
compress [x] = [x]
compress [x,y] = if x == y then [x] else [x,y]
compress (x:y:zs) = if x == y then compress (y:zs) else x : compress (y:zs)

-- 9
pack :: Eq a => [a] -> [[a]]
-- pack = L.group
pack [] = []
pack lst@(x:xs) = takeWhile (== x) lst : pack (dropWhile (== x) lst)

-- 10
encode :: Eq a => [a] -> [(Int, a)]
encode = foldr (\x acc -> (length x, head x) : acc) [] . pack

-- 11
data Encoding a = Multiple Int a | Single a deriving Show
encodeModified :: Eq a => [a] -> [Encoding a]
encodeModified = map (\(n, x) -> if n == 1 then Single x else Multiple n x) . encode
